const TruckModel = require('./TruckModel');
const MessageStore = require('../MessageStore');
const MessageProxy = require('../MessageProxy');
const url =  require('url');
const licenceVerification = require("../Venkman/licence").verify;


module.exports = class TruckStore extends MessageStore {
    constructor(activeModules) {
        super();
        this.activeModules = activeModules;
    }

    async verify(request, success, fail) {
        const keys = this._getKeys(request);

        if(licenceVerification(keys.licence)){
            return request;
        } else {
            throw {request, reason: "Invalid licence"};
        }


        if(this.activeModules.isActive(keys.sessionKey)){
            return request;
        } else {
            throw {request, reason: "Invalid session"};
        }
    }

    add(request, connection) {

        const model = new TruckModel(connection, new MessageProxy(connection), this._getKeys(request).sessionKey);

        this.bind(request, connection, model);
    }

    listActive() {
        return Object.keys(this.items).map((t) => {
            return this.items[t].obj
        });
    }

    _getKeys(request){
        var query = url.parse(request.httpRequest.url, true).query;
        const licence = request.httpRequest.headers["x-licence-key"] || query["x-licence-key"];
        const sessionKey = request.httpRequest.headers["x-session-key"] || query["x-session-key"];
        return {licence, sessionKey};
    }

}
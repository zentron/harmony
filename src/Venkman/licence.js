const keys = ["ABC"];

module.exports.verify = function(key) {
    return key !== null && keys.indexOf(key) !== -1
}
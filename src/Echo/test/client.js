var WebSocketClient = require('websocket').client;

var client = new WebSocketClient();

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

client.on('connectFailed', (error) => {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {

    connection.on('error', (error) =>
        console.log("Connection Error: " + error.toString())
    );

    connection.on('close', () => {
        client.connect('ws://localhost:8080/', 'json', null);
        console.log('Connection Closed');
    });

    rl.on('line', (input) => {
        if(!connection.connected)
            return;

        connection.sendUTF(JSON.stringify({msg: "readline", data: input} ))
    });

    connection.on('message', (message) => {
        if (message.type === 'utf8') {
            console.log("Server Replied: ", message.utf8Data);
        }
    });
});

client.connect('ws://localhost:8080/', 'json', null);
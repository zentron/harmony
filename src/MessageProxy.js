var EventEmitter = require('events').EventEmitter;
var util = require('util');

module.exports = class ProxyModel extends EventEmitter{
    constructor(connection) {
        super();
        EventEmitter.call(this);
        this.connection = connection;
        this._bind();
    }

    _messageHandler(message) {
        if (message.type === 'binary') {
            console.warn("Not expecting binary data. Message ignored");
            //console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            return;
        }

        if (message.type === 'utf8') {
            console.log("Incomming message: ", message.utf8Data)
            let msg, data = null;
            try{
                const body = JSON.parse(message.utf8Data);
                msg = body.msg; //would be nice to use spread operator
                data = body.data
            }catch(e) {
                console.warn("Received incorrectly formatted message.", e);
            }

            if(msg != null) {   
                this.emit(msg, data);
                this.emit("*", {msg, data});
            }
           
        }
    }

    _bind(){
        const messageHander = this._messageHandler.bind(this);
        this.connection.on('message', messageHander);

        this.connection.once('close', (reasonCode, description) => {
            this.removeAllListeners(); // No more proxy events will bubble up
            this.connection.removeListener('message', messageHander);
        });
    }

    send(msg, data) {        
        if(typeof data === "function") 
            throw "Unable to send functions!";
        
        //if(typeof data !== "object" && data !== undefined)
        //    data = {data};

        const payload = data === undefined ? {msg} : {msg, data}
        this.connection.sendUTF(JSON.stringify(payload));
    }
}

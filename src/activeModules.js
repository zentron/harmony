
module.exports = class ActiveModules {
    constructor() {
        this.sessions = [];
    }

    isActive() {
        return this.sessions.indexOf(id) !== -1;
    }

    start() {
        const id = +(new Date());
        this.sessions.push(id);
        return id;
    }

    stop(id) {
        const idx = this.sessions.indexOf(id);
        this.sessions.splice(idx, 1);
    }
};
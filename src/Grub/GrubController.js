module.exports = class GrubController {

    constructor(handlers, activeModules) {
        this.handlers = handlers;
        this.activeModules = activeModules;
    }

    async onListEndpoints(req, resp) {
        return this.handlers.venkman.store.listAvailable().map(t => {return {
            name: t.name,
            description: t.description,
            compositors: t.videoCompositors
        }});
    }

    async onListCompositors(req, resp){
        const body = req.body;

        const venkman = this.handlers.venkman.store.listAvailable().find(t => t.name === body.name);
        if(!venkman)
            return;

        return venkman.videoCompositors;
    }

    async onStartSession(req, resp) {
        const body = req.body;

        const venkman = this.handlers.venkman.store.listAvailable().find(t => t.name === body.name);
        if(!venkman)
            return;

        venkman.openUrl(body.compositorKey);
    }

    async onListSessions(req, resp) {
        const body = req.body;

        return this.handlers.truck.store.listActive().map(t => { return {
            latency: t.latency,
            browserData:   t.browserData,
            sessionKey:   t.sessionKey
        }});
    }

    async onSendData(req, resp) {
        const body = req.body;

        const truck = this.handlers.truck.store.listActive().find(t => t.sessionKey === body.sessionKey);

        if(truck)
        truck.sendData({setText: body.text});
    }

};
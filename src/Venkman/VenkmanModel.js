module.exports = class VenkmanModel {
    constructor(connection, proxy) {
        this.proxy = proxy;
        //this.address = connection.remoteAddress[0];
        this.videoCompositors = {};
        this._bind();
    }

    _bind() {
        this.proxy.on('name', this._onName.bind(this));
        this.proxy.on('addedVideoCompositor', this._onAddedVideoCompositor.bind(this));
        this.proxy.on('removedVideoCompositor', this._onRemovedVideoCompositor.bind(this));
    }

    _onAddedVideoCompositor(data) {
        if(!data.compositorKey){
            throw "Invalid data";
        }
        this.videoCompositors[data.compositorKey] = data;
    }
    _onRemovedVideoCompositor(data){
        delete this.videoCompositors[data.compositorKey];
    }

    _onName(data) {
        this.name = data.name;
        this.description = data.description;
    } 

    openUrl(compositorKey) {
        const sessionKey = +(new Date());
        this.proxy.send('open', { compositorKey: compositorKey, sessionKey: sessionKey });
        //this.connection.sendUTF(JSON.stringify({layerId, openUrl: 'http://blah'}));
    }

    closeUrl(layerId) {
        this.connection.sendUTF(JSON.stringify({layerId}));
    }

    setUID(){
        this.uid = +(new Date());
        this.proxy.send("uid", uid);
    }

    updateLicence() {
        this.proxy.send('name');
        //this.connection.sendUTF(JSON.stringify({msg: 'licence'}));
    }  
}

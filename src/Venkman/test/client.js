#!/usr/bin/env node

var WebSocketClient = require('websocket').client;

var client = new WebSocketClient();
const licenceKey = "ABC";

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {

    connection.on('error', (error) => 
        console.log("Connection Error: " + error.toString())
    );
    connection.on('close', () => console.log('Connection Closed'));

    connection.on('message', (message) => {
        if (message.type === 'utf8') {
            console.log("Incoming Message", message.utf8Data);
            var data = JSON.parse(message.utf8Data);
        switch(data.msg){
            case "name":
                sendName(connection);
                break;
            case "open":
                openUrl(connection, data.data);
                break;
            default:
                console.warn("Unrecognized message", data);
        }
    }

    setTimeout(() => registerCompositor(connection), 1500);
});


});

const chrome_open = require('../../util').chrome_open;

function openUrl(connection, data) {
    console.log("opening... ", data);
    if(data.compositorKey === "browser") { //Test case
        const sessionKey = data.sessionKey;
        var path = require('path');

        var grub = path.join(__dirname, "../../Truck/test/client.html?x-licence-key="+ licenceKey+"&x-session-key="+ sessionKey);
        chrome_open("file://"+ grub);//, {app: 'firefox'});
    } else {
        ///Connect to appropriate casper endpoint
    }
}

function registerCompositor(connection) {
    if(!connection.connected) 
        return;
    const data =  { compositorKey: 'browser', res: 1800, description: "A browser for simulating casper." };
    connection.sendUTF(JSON.stringify({msg: "addedVideoCompositor", data} ))
} 

function sendName(connection) {
    if(!connection.connected) 
        return;
    const data = {name: "Racing Terminal 1", description: "Video truck parked at north end."};
    connection.sendUTF(JSON.stringify({msg: "name", data}));
}

client.connect('ws://localhost:8080/venkman?X-Licence-Key='+ licenceKey, 'json', null);
module.exports = class EchoModel {
    constructor(connection, proxy) {
        this.proxy = proxy;
        this._bind();
    }

    _bind() {
        this.proxy.on('*', this._any.bind(this))
    }

    _any({msg, data}) {
        console.log((new Date()) + ' Echo ', msg, data);
        this.proxy.send(msg, data);
        
    }
}

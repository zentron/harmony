const express = require('express');
const url = require('url');
const GrubController = require("./Grub/GrubController");
var bodyParser = require('body-parser');
const app = express();

module.exports.start = function start(handlers, activeModules) {
    const controller = new GrubController(handlers, activeModules);

    const grubApp = express();

    grubApp.use(bodyParser.urlencoded({ extended: false }));
    grubApp.use(bodyParser.json());

    grubApp.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    grubApp.all('/*', function (req, res) {

            const method = mapRequestToMethod(req, controller);
            if (method) {
                method(req, res).then(result => {
                    res.send(result);
                }, err => {
                    console.error(err);
                    res.status(500).send(err)
                });
            } else {
                res.status(404).send('Unknown path');
            }
        }
    );

    app.use('/grub', grubApp);


    // app.get('/', function (req, res) {
    //     res.send('Hello World!')
    // });


    app.listen(8081, function (a) {
        console.log('HTTP endpoint listening on port 8081')
    });
};

function mapRequestToMethod(request, controller){
    const path = url.parse(request.url).pathname;
    const sep = path.indexOf("/", 1);
    const action = path.substr(1, sep === -1 ? undefined : sep);

    const reg = new RegExp(`on${action}`, "i");
    const methodName = Reflect.ownKeys(Reflect.getPrototypeOf(controller))
        .find((prop) => {
            return prop.match(reg);
        });

    if(!methodName)
        return;

    return controller[methodName].bind(controller);
}
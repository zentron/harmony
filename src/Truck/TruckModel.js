
const fakeData =  [
    {name: "Almandin", jockey:"Kerrin McEvoy", time:"3:20.58"},
    {name: "Prince of Penzance", jockey:"Michelle Payne", time:"3:23.15"},
    {name: "Protectionist", jockey:"Ryan Moore", time:"3:17.71"},
    {name: "Fiorente", jockey:"Damien Oliver", time:"3:20.30"},
    {name: "Green Moon", jockey:"Brett Prebble", time:"3:20.45"},
    {name: "Dunaden", jockey:"Christophe Lemaire", time:"3:20.84"},
    {name: "Americain", jockey:"Gérald Mossé", time:"3:26.87"}
];


module.exports = class TruckModel {
    constructor(connection, proxy, sessionKey) {
        this.proxy = proxy;
        this._bind();
        this.sessionKey = sessionKey;
    }

    _bind() {
        this.proxy.on('ready', this._onReady.bind(this));
        this.proxy.on('ping', this._onPing.bind(this))
        this.proxy.on('latency', this._onLatency.bind(this))
        //this.proxy.on('addedVideoCompositor', this._onAddedVideoCompositor.bind(this))
        //this.proxy.on('removedVideoCompositor', this._onRemovedVideoCompositor.bind(this))
    }

    _onPing(sentDate){
        this.proxy.send("pong", sentDate);
    }

    _onLatency(latency){
        this.latency = latency;
        console.log("latency: "+ latency);
    }

    _onReady(browserData) {
        this.browserData = browserData;
        console.log("ready", browserData);
    }

    sendData(data) {
        this.proxy.send("data", data);
    }

}

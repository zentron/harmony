const EchoModel = require('./EchoModel');
const MessageStore = require('../MessageStore');
const MessageProxy = require('../MessageProxy');


module.exports = class EchoStore extends MessageStore {

    async verify(request) {
        return request;
    }

     add(request, connection) {
         const m = new EchoModel(connection, new MessageProxy(connection));
        this.bind(request, connection, m);
     }
}
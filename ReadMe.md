## Install ##
> npm install


## Communication Phases ##

### Phase 1: Registration ###
A given client may have many locations available at any given time to take rendering requests. The registration phase ensures that the user is able to select the appropriate site(s) to perform the rendering operations against.

    1. `Venkman` process is runs with specific `licence-key` to identify the user/client. When `Venkman` starts up it opens a websocket connection to Harmony and passes up the `licence-key` for identification.
    2. `Venkman` detects available caspar\browser compositors running on system and sends information to `Harmony`.
    
```
                              +---------------------------------------------+
                              |                                             |
+----------------+            |                               +---------+   |
|                |            |                         +----->         |   |
|                |            |                         |     | Browser |   |
|                |            |   +---------------+     |     |         |   |
|                | 1. Licence Key |               |  2  |     +---------+   |
|    Harmony     <------------+---+    Venkman    +-----+                   |
|                |            |   |               |     |                   |
|                |            |   +---------------+     |                   |
|                |            |                         |    +----------+   |
|                |            |                         |    |          |   |
+----------------+            |                         |    |  Caspar  |   |
                              |                         +---->          |   |
                              |                              +----------+   |
                              |                                             |
                              +---------------------------------------------+
```

### Phase 2: Initiating session. ###
When a user requests a rendering session to start, the appropriate Caspar instance needs to start rendering Truck, using some identification so that multiple active sessions can be managed at once.

    1. Using `Grub` user requests list of available endpoints available to their account (connected `Venkman` endpoints)
    2. The user indicates through `Grub` which endpoint to connect to and start rendering a channel\layer.
    3. `Harmony` generates a `session-key` which it sends to the requested `Venkman` endpoint along with the start request.
    4. `Venkman` instructs caspar to start rendering a webpage (likely hosted by `Venkman` itself) passing through the `session-key` provided by `Harmony`.
    5. The page that loads (Truck\Trevor) opens a WebSocket connection back to `Harmony`. The provided `session-key` is verified against active sessions.
    6. The user is informed that the layer is now active and ready to take further rendering instruction.
    7. The user instructs the loaded Truck instance to load the appropriate module(s) (e.g "TAB Racing"). The relevant assets are set up on `Truck` however nothing is necessarily yet visible.
```
                                                               +-----------------------+
                                                               |                       |
                                                               |                       |
                                                               |    +-------------+    |
                                                         5. KEY|    |             |    |
                                                   +----------------+             |    |
                                                   |           |    |   Caspar    |    |
+---------------+       1. List      +-------------v--+        |    |  (Truck)    |    |
|               <--------------------+                |    7.  |    |             |    |
|               |                    |                +------------->             |    |
|               |       2. Open      |                |        |    +------^------+    |
|               +--------------------+                |        |           |           |
|      Grub     |                    |     Harmony    |        |           |4. KEY     |
|               |       6.Ready      |                |        |           |           |
|               <--------------------+                |        |    +------+------+    |
|               |                    |                |        |    |             |    |
|               |   7. Load Module   |                |        |    |             |    |
|               +-------------------->                |        |    |             |    |
+---------------+                    +--------------+-+        |    |   Venkman   |    |
                                                    |    3. KEY|    |             |    |
                                                    +--------------->             |    |
                                                               |    |             |    |
                                                               |    +-------------+    |
                                                               |                       |
                                                               +-----------------------+
```

### Phase 3. Live rendering ###
The most important phase, the user interacts with Grub to update the rendering layer on Truck.

    1. The user can show\hide modules or UI elements on demand,
    2. The user can manually enter data or pass data offered by `Harmony` relevant to that module up to Truck
    3. An external process receives notice that new racing odds have been provided. Depending on data\settings one of the following may occur
        A. User is provided with updated information to action as requested.
        B. The data is passed straight through to Truck
        
```

                                     +--------------+
                                     |              |
                                     |              |
                                     |     ETL      |
                                     |              |
                                     |              |
                                     +------+-------+
                                            |
                                            |
                                            |3. New Odds
                                            |                             +-----------------+
+---------------+                    +------v---------+                   |                 |
|               |                    |                |                   |                 |
|               |   1. Show\Hide Module               |                   |                 |
|               +--------------------+----------------------------------->|                 |
|      Grub     |                    |     Harmony    |                   |      Truck      |
|               |   2. Show Race Info|                |                   |                 |
|               +-------------------------------------------------------->|                 |
|               |                    |                |                   |                 |
|               |   3.A              |                |      3.B          |                 |
|               <--------------------+                +------------------>|                 |
+---------------+                    +----------------+                   |                 |
                                                                          +-----------------+
```

## Testing ##
To start the Harmony instance run

> npm start

This will start a WebSocket endpoint on 8080 and HTTP endpoint on 8081 (todo: consolidate to single port)

A dummy Venkman process (to be further expanded upon separately) can be started via

> npm run test-venkman

This will connect to the `ws://localhost:8080` endpoint created above and provide a chrome browser as a compositor option.

start a dummy grub console by running

> npm run test-grub

This will open a chrome window (tested on Mac, not working on windows atm ;) that connects to harmony over `http://localhost:8081` and loads the available venkman instances. Each time you click `Open Session` a new window will open. This represents the dummy truck instance which itself connects back to `Harmony` on `ws://localhost:8080`. Click `Refresh` under "Active Trucks". The `Grub` session can now send some text to the `Truck` instance, passed via `Harmony`.
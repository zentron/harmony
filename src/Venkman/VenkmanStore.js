const VenkmanModel = require('./VenkmanModel');
const MessageStore = require('../MessageStore');
const MessageProxy = require('../MessageProxy');

const licenceVerification = require("./licence").verify;


module.exports = class VenkmanStore extends MessageStore {
    constructor(activeModules) {
        super();
        this.activeModules = activeModules;
    }


    listAvailable() {
        return Object.keys(this.items).map((t) => {

            return this.items[t].obj
        });
    }

    async verify(request) {
        var url = require('url').parse(request.httpRequest.url, true).query;
        const key = request.httpRequest.headers["x-licence-key"] || url["X-Licence-Key"];
        if(licenceVerification(key)){
           return request;
        } else {
            throw {request, reason: "Invalid licence"};
        }
    }

     add(request, connection) {
        const v = new VenkmanModel(connection, new MessageProxy(connection), this.activeModules);
        this.bind(request, connection, v);
        v.updateLicence();
        //setTimeout(() => v.openUrl("SDDSD"), 2000);
      }

}
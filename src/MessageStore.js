module.exports = class MessageStore {
    constructor(){
        this.items = {};
    }

    async verify(request) {
        return request;
    }

    bind(request, connection, obj) {
        var key = JSON.stringify(connection.socket._peername);
        this.items[key] = {connection, obj};
        connection.once('close', (reasonCode, description) => {
            const item = this.items[key];
            delete this.items[key];
        });
    }
}
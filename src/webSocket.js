var WebSocketServer = require('websocket').server;
var http = require('http');

/*
 var server = http.createServer((request, response) => {
 console.log((new Date()) + ' Received request for ' + request.url);
 response.writeHead(404);
 response.end();
 });

 server.listen(8080, () => {
 console.log((new Date()) + ' Server is listening on port 8080')
 });

 */


module.exports.start = function start(handlers) {
    const server = http.createServer();
    server.listen(8080, () => {
        console.log((new Date()) + ' WS is listening on port 8080')
    });


    const wsServer = new WebSocketServer({
        httpServer: server,
        autoAcceptConnections: false
    });

    function originIsAllowed(origin) {
        // put logic here to detect whether the specified origin is allowed.
        return true;
    }

    wsServer.on('request', function (request) {

        request.once('requestRejected', (e) => console.log("Rejected"));
        request.once('requestAccepted', () => console.log("Accepted"));

        if (!originIsAllowed(request.origin)) {
            // Make sure we only accept requests from an allowed origin

            request.reject();
            console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
            return;
        }

        const validProtocols = ["json", "xml"];
        if (!request.requestedProtocols || request.requestedProtocols.length === 0) {
            //const assumeJson;
        } else {

        }

        let path = request.resourceURL.pathname;
        if (path === "/" || path === "") {
            path = "/echo";
        }

        const handlerName = Object.keys(handlers).find((handler) => {
            return (path === `/${handler}` || path.startsWith(`/${handler}?`))
        });
        if (!handlerName) {

            request.reject(404, `Endpoint not recognized: '${request.resourceURL.path}`);
            return;
        }
        const handler = handlers[handlerName];
        const store = handler.store;

        store.verify(request).then((request) => {
            request.once('requestAccepted', (conn) => store.add(request, conn));
            const connection = request.accept('json', request.origin);
            connection.on('error', function (err) {
                console.error("Something went wrong", err);
            });
            connection.on('close', function (reasonCode, description) {
                console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
            });

        }, (err) => {
            if(err.reason) {
                request.reject(403, err.reason);
            } else {
                console.error(err)
                request.reject(500, err.message || "");
            }

        });
    });
}
#!/usr/bin/env node


// listen for casper
// listen for service
// api for etl
// api for 
// deal with versions?
//graceful shutdown


const VenkmanStore = require('./Venkman/VenkmanStore');
const EchoStore = require('./Echo/EchoStore');
const TruckStore = require('./Truck/TruckStore');

const activeModules = new (require('./activeModules'))();


const handlers = {
    "venkman": {
        store: new VenkmanStore(activeModules),
    },
    "echo": {
        store: new EchoStore()
    },
    "truck": {
        store: new TruckStore(activeModules)
    }

};

const webSocketServer = require('./webSocket');
const webApiServer = require('./webApi');


//TODO: Combine these onto a single connection
webSocketServer.start(handlers);
webApiServer.start(handlers, activeModules);